import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import AppContainer from './src/navigation/AppContainer';
import { Provider } from 'unstated';

export default class App extends Component {

  render() {
    return (
      <Provider>
        <StatusBar barStyle="light-content" />
        <AppContainer />
      </Provider>
    );
  }
}