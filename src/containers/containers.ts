import { PersistContainer, PersistConfig } from 'unstated-persist';
import { ProfilesState, AppProfile } from '../common/interfaces';
import { AsyncStorage } from 'react-native';
import { updateSelectedProfile, editAppProfile } from '../common/genericFunctions';

class ProfilesContainer extends PersistContainer<ProfilesState> {
  state: ProfilesState = { profiles: [], selectedProfileID: null };
  persist: PersistConfig = {
    key: 'profiles',
    version: 1,
    storage: AsyncStorage
  };
  addProfile = (profile: AppProfile) => {
    this.setState((state: ProfilesState) => {
      let newProfiles: Array<AppProfile> = state.profiles;
      newProfiles.push(profile);
      return {
        ...state,
        profiles: newProfiles
      };
    });
  };
  editProfile = (newProfile: AppProfile) => {
    this.setState((state: ProfilesState) => {
      return {
        profiles: editAppProfile(newProfile, state.profiles)
      };
    });
  };
  selectProfile = (profileID: string) => {
    this.setState((state: ProfilesState) => {
      return {
        ...state,
        profiles: updateSelectedProfile(state.selectedProfileID, profileID, state.profiles),
        selectedProfileID: profileID
      };
    });
  };
  removeProfile = (profileID: string) => {
    this.setState((state: ProfilesState) => {
      let newProfiles: Array<AppProfile> = state.profiles;
      let isSelected: boolean = false;
      for(var i:number = 0; i < newProfiles.length; i++) {
        if(newProfiles[i].id === profileID) {
          isSelected = newProfiles[i].id === state.selectedProfileID;
          newProfiles.splice(i, 1);
          break;
        }
      }
      if(isSelected) {
        return {
          ...state,
          profiles: newProfiles,
          selectedProfileID: null
        }
      }
      return {
        ...state,
        profiles: newProfiles
      };
    });
  };
}

export { ProfilesContainer };