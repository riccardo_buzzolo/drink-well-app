export const Color = {
    PrimaryBlack: '#212121',
    BlackTransparent: 'rgba(0, 0, 0, 0.8)',
    Transparent: 'rgba(0, 0, 0, 0)',
    PrimaryWhite: '#FAFAFA',
    PrimaryOrange: '#FF9100',
    PrimaryRed: '#D50000'
};