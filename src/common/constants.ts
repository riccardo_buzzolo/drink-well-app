import { Dimensions, Platform } from "react-native";
import { Translation } from "./translations";
import { Error } from '../common/interfaces';

const TOP_TAB_WIDTH = Number((Dimensions.get('screen').width).toFixed(0));
const KEYBOARD_VERTICAL_OFFSET = Platform.OS === 'ios' ? 80 : 0;
const INPUT_LABEL_WIDTH: number = Number((Dimensions.get('screen').width / 2.5).toFixed(0));
const AVATAR: number = 200;

const AvatarSource = {
    MALE: require('../../assets/images/man.png'),
    FEMALE: require('../../assets/images/woman.png')
};

const RouteName = {
    Home: 'Home',
    Profile: 'Profile',
    ProfileMain: 'ProfileMain',
    ProfileAddEdit: 'ProfileAddEdit',
    Result: 'Result',
    Camera: 'Camera'
};

const WrongUsername: Error = { type: 1 };
const WrongAge: Error = { type: 2, message: Translation.AgeErrorMessage };
const WrongHeight: Error = { type: 3 };
const WrongWeight: Error = { type: 4 };
const NotSelectedSex: Error = { type: 5, message: Translation.SexErrorMessage };
const NotSelectedLicence: Error = { type: 6, message: Translation.LicenceErrorMessage };


export {
    AvatarSource,
    WrongUsername,
    WrongAge,
    WrongHeight,
    WrongWeight,
    NotSelectedSex,
    NotSelectedLicence,
    RouteName,
    TOP_TAB_WIDTH,
    KEYBOARD_VERTICAL_OFFSET,
    INPUT_LABEL_WIDTH,
    AVATAR
};