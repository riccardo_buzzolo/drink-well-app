import { ImageSourcePropType } from "react-native";

enum Option {
    CAMERA = 'camera',
    GALLERY = 'gallery',
    REMOVE = 'remove'
}

enum Sex {
    MALE = 'Maschio',
    FEMALE = 'Femmina'
}

enum Licence {
    STANDARD = 'Standard',
    NEWBIE = 'Newbie'
}

interface Profile {
    id: string
    avatar: ImageSourcePropType
    username: string
}

interface AppProfile extends Profile {
    age: number
    height: number
    weight: number
    sex: Sex
    licence: Licence
    isSelected: boolean
}

interface InputProfile extends Profile {
    inputAge: string
    inputHeight: string
    inputWeight: string
    inputSex?: number
    inputLicence?: number
}

interface Error {
    type: number,
    message?: string
}

// CONTAINER TYPES
interface ProfilesState {
    profiles: Array<AppProfile>
    selectedProfileID?: string
}

export { ProfilesState, Profile, AppProfile, InputProfile, Licence, Sex, Option, Error };