import { Alert } from "react-native";
import * as ImagePicker from 'expo-image-picker';
import { AppProfile, InputProfile, Sex, Licence } from "./interfaces";
import { Translation } from "./translations";
import { AvatarSource } from "./constants";

const takePictureFromCamera = async (): Promise<ImagePicker.ImagePickerResult> => {
  let result: ImagePicker.ImagePickerResult = await ImagePicker.launchCameraAsync({
    mediaTypes: ImagePicker.MediaTypeOptions.Images,
    quality: 0.8,
    allowsEditing: true,
    aspect: [1, 1]
  });

  if (!result.cancelled) {
    return result;
  }
  return null;
};

const pickImageFromGallery = async (): Promise<ImagePicker.ImagePickerResult> => {
  let result: ImagePicker.ImagePickerResult = await ImagePicker.launchImageLibraryAsync({
    mediaTypes: ImagePicker.MediaTypeOptions.Images,
    quality: 0.8,
    allowsEditing: true,
    aspect: [1, 1]
  });

  if (!result.cancelled) {
    return result;
  }
  return null;
};

const showDialogYesNo = (message: string, onYes: () => void, onNo: () => void) => Alert.alert(
    '',
    message,
    [
      { text: Translation.No, onPress: onNo },
      { text: Translation.Yes, onPress: onYes },
    ],
    { cancelable: false }
);

const showDialog = (message: string, onOk: () => void) => Alert.alert(
  '',
  message,
  [
    { text: 'Ok', onPress: () => onOk },
  ],
  { cancelable: false }
);

const createAppProfile = (inputProfile: InputProfile): AppProfile => ({
    id: inputProfile.id,
    username: inputProfile.username,
    avatar: inputProfile.avatar,
    age: Number(inputProfile.inputAge),
    height: Number(inputProfile.inputHeight),
    weight: Number(inputProfile.inputWeight),
    sex: inputProfile.inputSex === 1 ? Sex.FEMALE : Sex.MALE,
    licence: inputProfile.inputLicence === 1 ? Licence.NEWBIE : Licence.STANDARD,
    isSelected: false
});

const initializeInputProfile = () => ({
  id: (Math.random() * 10000).toFixed(0),
  avatar: AvatarSource.MALE,
  username: '',
  inputAge: '',
  inputHeight: '',
  inputWeight: '',
  inputSex: null,
  inputLicence: null,
});

const createInputProfile = (profileToEdit: AppProfile) => ({
  id: profileToEdit.id,
  avatar: profileToEdit.avatar,
  username: profileToEdit.username,
  inputAge: profileToEdit.age.toString(),
  inputHeight: profileToEdit.height.toString(),
  inputWeight: profileToEdit.weight.toString(),
  inputSex: profileToEdit.sex === Sex.MALE ? 0 : 1,
  inputLicence: profileToEdit.licence === Licence.STANDARD ? 0 : 1
});

const editAppProfile = (newProfile: AppProfile, profiles: Array<AppProfile>) => profiles.map((profile: AppProfile) => {
  if(profile.id === newProfile.id) {
    return {
      ...profile,
      avatar: newProfile.avatar,
      username: newProfile.username,
      age: newProfile.age,
      height: newProfile.height,
      weight: newProfile.weight,
      sex: newProfile.sex,
      licence: newProfile.licence
    };
  }
  return profile;
});

const updateSelectedProfile = (oldProfileID: string, newProfileID: string, profiles: Array<AppProfile>): Array<AppProfile> => profiles.map((profile: AppProfile) => {
  if(oldProfileID !== newProfileID) {
    if(oldProfileID && oldProfileID === profile.id) {
      return { ...profile, isSelected: false };
    } else if(newProfileID === profile.id) {
      return { ...profile, isSelected: true };
    } else {
      return profile;
    }
  }
  return profile;
});

export {
  showDialog,
  showDialogYesNo,
  initializeInputProfile,
  createAppProfile,
  editAppProfile,
  createInputProfile,
  updateSelectedProfile,
  pickImageFromGallery,
  takePictureFromCamera
};