import React from 'react';
import { StyleSheet, Platform, TouchableOpacity, GestureResponderEvent } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

type BackButtonProps = {
    color: string,
    onPress: (event: GestureResponderEvent) => void
};

const BackButton = (props: BackButtonProps) => {
    const { color, onPress }: BackButtonProps = props;
    return (
        <TouchableOpacity
            style={[styles.container, { backgroundColor: 'rgba(0,0,0,0)' }]}
            activeOpacity={1}
            onPress={onPress}
        >
            <Ionicons style={styles.icon} name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'} size={28} color={color} />
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon: {
        marginHorizontal: 25
    }
});

export default BackButton;