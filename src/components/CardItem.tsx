import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, GestureResponderEvent } from 'react-native';
import { Color } from '../common/colors';

type CardItemProps = {
    text: string
    width: number
    onPress: (event: GestureResponderEvent) => void
};

const CardItem = (props: CardItemProps) => {
    const { text, onPress, width } = props;
    return (
        <TouchableOpacity
            style={[styles.container, { width }]}
            onPress={onPress}
        >
            <Text style={styles.optText}>{text}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: Color.PrimaryWhite
    },
    optText: {
        fontSize: 18,
        marginHorizontal: 20,
        marginVertical: 15
    }
});

export default CardItem;