import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { ProfilesState, AppProfile } from '../common/interfaces';
import { RouteName } from '../common/constants';
import ProfileCell from './ProfileCell';
import { Color } from '../common/colors';
import AddButton from './AddButton';
import { NavigationScreenProp } from 'react-navigation';
import { Translation } from '../common/translations';
import { ProfilesContainer } from '../containers/containers';
import { Subscribe } from 'unstated';

type ProfileScreenProps = {
    navigation: NavigationScreenProp<any, any>
};

class ProfileScreen extends Component<ProfileScreenProps> {

    _keyExtractor = (item: AppProfile) => item.id;

    _renderItem = (item: AppProfile, removeProfile: (profileID: string) => void, selectProfile: (profileID: string) => void) => (
        <ProfileCell
            profile={item}
            onCellPress={() => selectProfile(item.id)}
            onEditPress={() => this.editProfile(item, removeProfile)}
        />
    );

    editProfile = (profile: AppProfile, removeProfile: (profileID: string) => void) => {
        const { navigation }: ProfileScreenProps = this.props;
        navigation.navigate(RouteName.ProfileAddEdit, { profile, removeProfile });
    }

    addProfile = () => {
        const { navigation }: ProfileScreenProps = this.props;
        navigation.navigate(RouteName.ProfileAddEdit);
    }

    render() {
        return (
            <Subscribe to={[ProfilesContainer]}>
                {(profilesContainer: ProfilesContainer) => {
                    const { profiles }: ProfilesState = profilesContainer.state;
                    const { selectProfile, removeProfile }: ProfilesContainer = profilesContainer;
                    if(profiles.length === 0) {
                        return (
                            <View style={styles.emptyContainer}>
                                <Text style={styles.emptyListText}>{Translation.CreateNewProfile}</Text>
                                <AddButton
                                    color={Color.PrimaryBlack}
                                    onPress={this.addProfile}
                                />
                            </View>
                        );
                    }
                    return (
                        <View style={styles.container}>
                            <FlatList
                                data={profiles}
                                extraData={this.state}
                                keyExtractor={this._keyExtractor}
                                renderItem={({ item }: { item: AppProfile }) => this._renderItem(item, removeProfile, selectProfile)}
                                ItemSeparatorComponent={() => <View style={styles.itemSeparator} />}
                            />
                            <AddButton
                                color={Color.PrimaryBlack}
                                onPress={this.addProfile}
                            />
                        </View>
                    );
                }}
            </Subscribe>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    itemSeparator: {
        borderBottomWidth: 1,
        borderColor: Color.PrimaryBlack
    },
    emptyContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    emptyListText: {
        marginBottom: 30,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 24,
        width: 200
    }
});

export default ProfileScreen;