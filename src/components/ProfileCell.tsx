import React, { Component } from 'react';
import { Text, Image, StyleSheet, TouchableOpacity, GestureResponderEvent, View, Platform } from 'react-native';
import { AppProfile } from '../common/interfaces';
import { Color } from '../common/colors';
import { Ionicons } from '@expo/vector-icons';

type ProfileCellProps = {
    profile: AppProfile
    onCellPress: (event: GestureResponderEvent) => void
    onEditPress: (event: GestureResponderEvent) => void
};

class ProfileCell extends Component<ProfileCellProps> {

    render() {
        const { profile, onCellPress, onEditPress }: ProfileCellProps = this.props;
        return (
            <TouchableOpacity
                style={[styles.container, { backgroundColor: profile.isSelected ? Color.PrimaryOrange : Color.PrimaryWhite }]}
                activeOpacity={1}
                onPress={onCellPress}
            >
                <Image source={profile.avatar} style={[styles.avatar, { borderColor: profile.isSelected ? Color.PrimaryBlack : Color.PrimaryOrange }]} />
                <Text allowFontScaling style={styles.username}>{profile.username}</Text>
                <View>
                    <TouchableOpacity
                        style={styles.edit}
                        activeOpacity={0.8}
                        onPress={onEditPress}
                    >
                        <Ionicons name={Platform.OS === 'ios' ? 'ios-create' : 'md-create'} size={32} color={Color.PrimaryBlack} />
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    avatar: {
        borderWidth: 2,
        borderRadius: 30,
        width: 60,
        height: 60,
        marginHorizontal: 20,
        marginVertical: 15
    },
    username: {
        flex: 1,
        textAlign: 'left',
        fontSize: 22,
        fontWeight: 'bold'
    },
    edit: {
        margin: 15,
        padding: 10
    }
});

export default ProfileCell;