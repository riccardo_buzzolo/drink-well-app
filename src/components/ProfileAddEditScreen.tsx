import React, { Component } from 'react';
import { View, StyleSheet, Image, KeyboardAvoidingView, TouchableOpacity, Platform } from 'react-native';
import * as Permissions from 'expo-permissions';
import RNPickerSelect from 'react-native-picker-select';
import { Sex, Licence, Option, Error, InputProfile, AppProfile } from '../common/interfaces';
import { AvatarSource, AVATAR, KEYBOARD_VERTICAL_OFFSET, WrongUsername, WrongAge, WrongHeight, WrongWeight, NotSelectedSex, NotSelectedLicence } from '../common/constants';
import { Color } from '../common/colors';
import { NavigationScreenProp, ScrollView } from 'react-navigation';
import LabelTextInput from './LabelTextInput';
import { Translation } from '../common/translations';
import PrimaryButton from './PrimaryButton';
import ChooseAvatarModal from './ChooseAvatarModal';
import { Subscribe } from 'unstated';
import { ProfilesContainer } from '../containers/containers';
import { showDialog, createAppProfile, pickImageFromGallery, takePictureFromCamera, initializeInputProfile, createInputProfile } from '../common/genericFunctions';

type ProfileAddEditScreenProps = {
    navigation: NavigationScreenProp<any, any>
};

type ProfileAddEditScreenState = {
    profile: InputProfile
    visible: boolean
    error: Error
};

class ProfileAddEditScreen extends Component<ProfileAddEditScreenProps, ProfileAddEditScreenState> {

    constructor(props: ProfileAddEditScreenProps) {
        super(props);
        this.state = {
            profile: initializeInputProfile(),
            visible: false,
            error: null
        };
    }

    componentWillMount() {
        const { navigation }: ProfileAddEditScreenProps = this.props;
        this.getPermissionAsync();
        const profileToEdit: AppProfile = navigation.getParam('profile');
        if(profileToEdit) {
            this.setState({
                profile: createInputProfile(profileToEdit)
            });
        }
    }

    getPermissionAsync = async () => {
        if (Platform.OS === 'ios') {
          const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
          if (status !== 'granted') {
            showDialog(Translation.PermissionsDenied, () => {});
          }
        }
    }

    componentWillUnmount() {
        this.setState({});
    }

    toggleModal = () => {
        this.setState({ visible: !this.state.visible });
    }

    chooseOption = async (option: Option) => {
        if(option === Option.CAMERA) {
            let result = await takePictureFromCamera();
            if(result) {
                this.setState({ profile: { ...this.state.profile, avatar: { uri: result.uri } }})
            }
        } else if(option === Option.GALLERY) {
            let result = await pickImageFromGallery();
            if(result) {
                this.setState({ profile: { ...this.state.profile, avatar: { uri: result.uri } }})
            }
        } else if(option === Option.REMOVE) {
            const { profile }: ProfileAddEditScreenState = this.state;
            this.setState({
                profile: {
                    ...profile,
                    avatar: profile.inputSex === 1 && profile.inputSex !== null ? AvatarSource.FEMALE : AvatarSource.MALE
                }
            });
        }
        this.toggleModal();
    }

    handleUsernameChange = (username: string) => this.setState({ profile: { ...this.state.profile, username }})

    handleAgeChange = (inputAge: string) => this.setState({ profile: { ...this.state.profile, inputAge } });

    handleHeightChange = (inputHeight: string) => this.setState({ profile: { ...this.state.profile, inputHeight } });

    handleWeightChange = (inputWeight: string) => this.setState({ profile: { ...this.state.profile, inputWeight } });

    handleSexChange = (inputSex: number) => {
        const { profile }: ProfileAddEditScreenState = this.state;
        this.setState({
            error: null,
            profile: {
                ...profile,
                inputSex,
                avatar: profile.avatar === AvatarSource.MALE || profile.avatar === AvatarSource.FEMALE ?
                    (inputSex === 1 ? AvatarSource.FEMALE : AvatarSource.MALE) : profile.avatar
            }
        });
    }

    handleLicenceChange = (inputLicence: number) => this.setState({ error: null, profile: { ...this.state.profile, inputLicence } });

    validate = async (profile: InputProfile) => {
        if(profile.username === '') { this.setState({ error: WrongUsername }); return false; }
        if(Number(profile.inputAge) < 16) { this.setState({ error: WrongAge }); showDialog(WrongAge.message, this.clearError); return false; }
        if(Number(profile.inputHeight) <= 0) { this.setState({ error: WrongHeight }); return false; }
        if(Number(profile.inputWeight) <= 0) { this.setState({ error: WrongWeight }); return false; }
        if(profile.inputSex === null) { this.setState({ error: NotSelectedSex }); showDialog(NotSelectedSex.message, this.clearError); return false; }
        if(profile.inputLicence === null) { this.setState({ error: NotSelectedLicence }); showDialog(NotSelectedLicence.message, this.clearError); return false; }
        return true;        
    }

    onSaveProfile = async (addProfile: (profile: AppProfile) => any, editProfile: (newProfile: AppProfile) => void) => {
        const { profile }: ProfileAddEditScreenState = this.state;
        if(await this.validate(profile)) {
            const validProfile: AppProfile = createAppProfile(profile);
            const { navigation }: ProfileAddEditScreenProps = this.props;
            await navigation.getParam('profile') ? editProfile(validProfile) : addProfile(validProfile);
            await navigation.pop();
        }
    }

    clearError = () => {
        this.setState({ error: null });
    }

    render() {
        const { profile, error, visible }: ProfileAddEditScreenState = this.state;
        return (
            <Subscribe to={[ProfilesContainer]}>
                {(profilesContainer: ProfilesContainer) => {
                    const { addProfile, editProfile }: ProfilesContainer = profilesContainer;
                    return (
                        <View style={{ flex: 1 }}>
                            <ScrollView
                                keyboardDismissMode="interactive"
                                contentContainerStyle={{ flexGrow: 1 }}
                            >
                                <KeyboardAvoidingView behavior='position' keyboardVerticalOffset={KEYBOARD_VERTICAL_OFFSET}>
                                    <View style={styles.container}>
                                        <TouchableOpacity
                                            activeOpacity={1}
                                            onPress={this.toggleModal}
                                        >
                                            <Image
                                                source={profile ? profile.avatar : AvatarSource.MALE}
                                                style={styles.avatar}
                                            />
                                        </TouchableOpacity>
                                        <LabelTextInput
                                            keyboardType="default"
                                            containerStyle={styles.formInput}
                                            returnKeyType="done"
                                            label={Translation.Name}
                                            value={profile.username}
                                            error={error && error.type === WrongUsername.type}
                                            onChangeText={(username: string) => this.handleUsernameChange(username)}
                                            onFocus={this.clearError}
                                        />
                                        <LabelTextInput
                                            keyboardType="numeric"
                                            containerStyle={styles.formInput}
                                            returnKeyType="done"
                                            label={Translation.Age}
                                            value={profile.inputAge}
                                            error={error && error.type === WrongAge.type}
                                            onChangeText={(inputAge: string) => this.handleAgeChange(inputAge)}
                                            onFocus={this.clearError}
                                        />
                                        <LabelTextInput
                                            keyboardType="numeric"
                                            containerStyle={styles.formInput}
                                            returnKeyType="done"
                                            label={Translation.Height}
                                            value={profile.inputHeight}
                                            error={error && error.type === WrongHeight.type}
                                            onChangeText={(inputHeight: string) => this.handleHeightChange(inputHeight)}
                                            onFocus={this.clearError}
                                        />
                                        <LabelTextInput
                                            keyboardType="numeric"
                                            containerStyle={styles.formInput}
                                            returnKeyType="done"
                                            label={Translation.Weight}
                                            value={profile.inputWeight}
                                            error={error && error.type === WrongWeight.type}
                                            onChangeText={(inputWeight: string) => this.handleWeightChange(inputWeight)}
                                            onFocus={this.clearError}
                                        />
                                    </View>
                                </KeyboardAvoidingView>
                                <View style={{ flex: 1 }}>
                                    <RNPickerSelect
                                        placeholder={{
                                            label: Translation.Sex,
                                            value: null
                                        }}
                                        items={[
                                            { label: Sex.MALE, value: 0 },
                                            { label: Sex.FEMALE, value: 1 }
                                        ]}
                                        onValueChange={(inputSex: number) => this.handleSexChange(inputSex)}
                                        style={error && error.type === NotSelectedSex.type ? pickerErrorStyles : pickerSelectStyles}
                                        value={profile.inputSex}
                                    />
                                    <RNPickerSelect
                                        placeholder={{
                                            label: Translation.Licence,
                                            value: null
                                        }}
                                        items={[
                                            { label: Licence.STANDARD, value: 0 },
                                            { label: Licence.NEWBIE, value: 1 }
                                        ]}
                                        onValueChange={(inputLicence: number) => this.handleLicenceChange(inputLicence)}
                                        style={error && error.type === NotSelectedLicence.type ? pickerErrorStyles : pickerSelectStyles}
                                        value={profile.inputLicence}
                                    />
                                    <PrimaryButton
                                        title={Translation.SaveProfile}
                                        color={Color.PrimaryBlack}
                                        activeColor={Color.PrimaryOrange}
                                        onPress={async () => this.onSaveProfile(addProfile, editProfile)}
                                    />
                                </View>
                            </ScrollView>
                            <ChooseAvatarModal
                                visible={visible}
                                toggleModal={this.toggleModal}
                                chooseOption={this.chooseOption}
                            />
                        </View>
                    );
                }}
            </Subscribe>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    avatar: {
        height: AVATAR,
        width: AVATAR,
        borderWidth: 2,
        borderColor: Color.PrimaryOrange,
        marginVertical: 35,
        borderRadius: 100
    },
    formInput: {
        marginHorizontal: 15,
        marginVertical: 10
    }
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 10,
        marginHorizontal: 15,
        marginVertical: 10,
        borderWidth: 1,
        borderColor: Color.PrimaryBlack,
        borderRadius: 4,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        marginHorizontal: 15,
        marginVertical: 10,
        borderWidth: 0.5,
        borderColor: Color.PrimaryBlack,
        borderRadius: 8,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    pickerError: {
        borderColor: Color.PrimaryRed
    }
});

const pickerErrorStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 10,
        marginHorizontal: 15,
        marginVertical: 10,
        borderWidth: 1,
        borderColor: Color.PrimaryRed,
        borderRadius: 4,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        marginHorizontal: 15,
        marginVertical: 10,
        borderWidth: 0.5,
        borderColor: Color.PrimaryRed,
        borderRadius: 8,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    pickerError: {
        borderColor: Color.PrimaryRed
    }
});

export default ProfileAddEditScreen;