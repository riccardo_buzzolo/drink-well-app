import React from 'react';
import { StyleSheet, View, Text, Modal, TouchableOpacity, Platform, SafeAreaView } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Color } from '../common/colors';
import CardItem from './CardItem';
import { Translation } from '../common/translations';
import { Option } from '../common/interfaces';

type ChooseAvatarModalProps = {
    visible: boolean
    toggleModal: () => void
    chooseOption: (option: Option) => void
};

const ChooseAvatarModal = (props: ChooseAvatarModalProps) => {

    const toggle = () => {
        const { toggleModal }: ChooseAvatarModalProps = props;
        toggleModal();
    }

    const choose = (option: Option) => {
        const { chooseOption }: ChooseAvatarModalProps = props;
        chooseOption(option);
    }

    const { visible }: ChooseAvatarModalProps = props;
    return (
        <SafeAreaView style={{ backgroundColor: Color.PrimaryBlack }}>
            <Modal
                animationType='none'
                transparent={true}
                visible={visible}
                onRequestClose={toggle}
            >
                <View style={styles.container}>
                    <TouchableOpacity
                        style={styles.closeButton}
                        activeOpacity={0.8}
                        onPress={toggle}
                    >
                        <Ionicons style={styles.closeButtonImage} name={Platform.OS === 'ios' ? 'ios-close' : 'md-close'} size={34} color={Color.PrimaryWhite} />
                    </TouchableOpacity>
                    <View style={styles.optContainer}>
                        <View style={styles.options}>
                            <CardItem text={Translation.Camera} width={200} onPress={() => choose(Option.CAMERA)} />
                            <CardItem text={Translation.Gallery} width={200} onPress={() => choose(Option.GALLERY)} />
                            <CardItem text={Translation.Remove} width={200} onPress={() => choose(Option.REMOVE)} />
                        </View>
                    </View>
                </View>
            </Modal>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.BlackTransparent
    },
    optContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center' 
    },
    options: {
        marginHorizontal: 25,
        backgroundColor: Color.PrimaryWhite,
        borderRadius: 4,
        paddingVertical: 5
    },
    closeButton: {
        alignSelf: 'flex-end'
    },
    closeButtonImage: {
        marginTop: 30,
        marginRight: 25
    }
});

export default ChooseAvatarModal;