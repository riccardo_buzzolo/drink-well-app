import React from 'react';
import { StyleSheet, Platform, TouchableOpacity, GestureResponderEvent } from 'react-native';
import { Color } from '../common/colors';
import { Ionicons } from '@expo/vector-icons';

type AddButtonProps = {
    color: string,
    onPress: (event: GestureResponderEvent) => void
};

const AddButton = (props: AddButtonProps) => {
    const { color, onPress }: AddButtonProps = props;
    return (
        <TouchableOpacity
            style={[styles.container, { backgroundColor: color }]}
            activeOpacity={1}
            onPress={onPress}
        >
            <Ionicons style={styles.icon} name={Platform.OS === 'ios' ? 'ios-add' : 'md-add'} size={34} color={Color.PrimaryWhite} />
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 15,
        right: 15,
        height: 50,
        width: 50,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 4,
        shadowColor: '#000',
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        shadowOffset: {
            width: 0,
            height: 2
        }
    },
    icon: {
        marginTop: Platform.OS === 'ios' ? 4 : 0
    }
});

export default AddButton;