import React, { Component } from 'react';
import { Text, StyleSheet, TextInput, StyleProp, ViewStyle, ReturnKeyTypeOptions, KeyboardTypeOptions, Animated, Easing, TextStyle } from 'react-native';
import { Color } from '../common/colors';
import { INPUT_LABEL_WIDTH } from '../common/constants';
import { AnimatedValue } from 'react-navigation';

type LabelTextInputProps = {
    label: string
    value?: string
    error?: boolean
    containerStyle: StyleProp<ViewStyle>
    returnKeyType: ReturnKeyTypeOptions
    keyboardType: KeyboardTypeOptions
    onChangeText: (value: string) => void
    onFocus: () => void
};

type LabelTextInputState = {
    animatedValue: AnimatedValue,
};

class LabelTextInput extends Component<LabelTextInputProps, LabelTextInputState> {

    constructor(props: LabelTextInputProps) {
        super(props)
        this.state = {
            animatedValue: new Animated.Value(0)
        };
    }

    componentWillReceiveProps(nextProps: LabelTextInputProps) {
        if(nextProps.error) {
            this.handleAnimation();
        }
    }

    handleTextChange = (value: string) => {
        this.props.onChangeText(value);
    }

    handleAnimation = () => {
        // Animation consists of a sequence of steps
        Animated.sequence([
            // start rotation in one direction (only half the time is needed)
            Animated.timing(this.state.animatedValue, {toValue: 5.0, duration: 40, easing: Easing.linear, useNativeDriver: true}),
            // rotate in other direction, to minimum value (= twice the duration of above)
            Animated.timing(this.state.animatedValue, {toValue: -5.0, duration: 80, easing: Easing.linear, useNativeDriver: true}),
            // return to begin position
            Animated.timing(this.state.animatedValue, {toValue: 0.0, duration: 40, easing: Easing.linear, useNativeDriver: true})
        ]).start();
    }

    render() {
        const {
            value,
            label,
            error,
            containerStyle,
            returnKeyType,
            keyboardType,
            onFocus
        }: LabelTextInputProps = this.props;
        return (
            <Animated.View style={[
                styles.container,
                containerStyle,
                error ? styles.errorContainer : {},
                {
                    transform: [{
                        translateX: this.state.animatedValue
                    }]
                }
            ]}>
                <Text style={[styles.label, error ? styles.errorText : {}]}>{label}</Text>
                <TextInput
                    onFocus={onFocus}
                    returnKeyType={returnKeyType}
                    enablesReturnKeyAutomatically
                    keyboardType={keyboardType}
                    value={value}
                    onChangeText={this.handleTextChange}
                    style={styles.input}
                />
            </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: Color.PrimaryBlack
    },
    label: {
        width: INPUT_LABEL_WIDTH,
        fontSize: 18
    },
    input: {
        flex: 1,
        height: 30,
        width: undefined,
        paddingHorizontal: 5
    },
    errorContainer: {
        borderBottomColor: Color.PrimaryRed
    },
    errorText: {
        color: Color.PrimaryRed
    }
});

export default LabelTextInput;