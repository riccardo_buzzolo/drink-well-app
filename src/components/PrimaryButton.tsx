import React from 'react';
import { TouchableOpacity, GestureResponderEvent, Text, StyleSheet } from "react-native";
import { Color } from '../common/colors';

type PrimaryButtonProps = {
    title: string
    color: string
    activeColor: string
    onPress: (event: GestureResponderEvent) => void
};

const PrimaryButton = (props: PrimaryButtonProps) => {
    const { title, color, onPress }: PrimaryButtonProps = props;
    return (
        <TouchableOpacity
            activeOpacity={1}
            style={[styles.container, { backgroundColor: color }]}
            onPress={onPress}
        >
            <Text style={styles.title}>{title.toUpperCase()}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        margin: 15,
        padding: 0,
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 4,
        shadowColor: '#000',
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        shadowOffset: {
            width: 0,
            height: 2
        }
    },
    title: {
        color: Color.PrimaryWhite,
        fontSize: 14,
        marginVertical: 10,
        marginHorizontal: 15
    }
});

export default PrimaryButton;