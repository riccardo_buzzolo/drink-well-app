import React from 'react';
import { TouchableOpacity, GestureResponderEvent, Text, StyleSheet } from "react-native";
import { Color } from '../common/colors';

type TextButtonProps = {
    title: string
    color: string
    onPress: (event: GestureResponderEvent) => void
};

const TextButton = (props: TextButtonProps) => {
    const { title, color, onPress }: TextButtonProps = props;
    return (
        <TouchableOpacity
            activeOpacity={0.8}
            style={styles.container}
            onPress={onPress}
        >
            <Text style={[styles.title, { color }]}>{title}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 15,
        padding: 10,
        backgroundColor: Color.Transparent,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        minHeight: 15,
        fontSize: 14,
        textAlign: 'center',
        textAlignVertical: 'center'
    }
});

export default TextButton;