import React from 'react';
import { NavigationContainer, createStackNavigator, NavigationScreenOptions, NavigationScreenProp } from 'react-navigation';
import ProfileAddEditScreen from '../components/ProfileAddEditScreen';
import { Color } from '../common/colors';
import CameraScreen from '../components/CameraScreen';
import BackButton from '../components/BackButton';
import { Translation } from '../common/translations';
import TextButton from '../components/TextButton';
import { showDialogYesNo } from '../common/genericFunctions';

const ProfileAddEditStack: NavigationContainer = createStackNavigator(
    {
        Main: {
            screen: ProfileAddEditScreen,
            navigationOptions: ({ navigation }: { navigation: NavigationScreenProp<any, any> }): NavigationScreenOptions => ({
                headerTitle: navigation.getParam('profile') ? 'EditProfile' : 'Add Profile',
                headerTintColor: Color.PrimaryWhite,
                headerLeft: <BackButton color={Color.PrimaryWhite} onPress={() => navigation.pop()} />,
                headerRight: navigation.getParam('profile') ? (
                    <TextButton
                        title={Translation.Remove}
                        color={Color.PrimaryRed}
                        onPress={() => showDialogYesNo(
                            Translation.RemoveProfileQuestion,
                            () => {
                                navigation.state.params.removeProfile(navigation.getParam('profile').id)
                                navigation.pop();
                            },
                            () => {}
                        )}
                    />
                ) : null,
                headerStyle: {
                    height: 60,
                    backgroundColor: Color.PrimaryBlack,
                    borderBottomColor: Color.PrimaryBlack,
                    elevation: 4,
                    shadowColor: '#000',
                    shadowOpacity: 0.23,
                    shadowRadius: 2.62,
                    shadowOffset: {
                        width: 0,
                        height: 2
                    }
                }
            })
        },
        Camera: {
            screen: CameraScreen,
            navigationOptions: (): NavigationScreenOptions => ({
                header: null
            })
        }
    },
    {
        initialRouteName: 'Main',
        headerMode: 'screen'
    }
);

export default ProfileAddEditStack;