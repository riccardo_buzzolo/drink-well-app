import React from 'react';
import { NavigationContainer, createAppContainer, createStackNavigator, NavigationScreenOptions } from "react-navigation";
import AppNavigator from "./AppNavigator";
import { Color } from "../common/colors";
import AppHeader from "./AppHeader";
import ProfileAddEditStack from './ProfileAddEditStack';

const ModalStack: NavigationContainer = createStackNavigator(
    {
        Main: {
            screen: AppNavigator,
            navigationOptions: (): NavigationScreenOptions => ({
                header: <AppHeader />
            })
        },
        ProfileAddEdit: {
            screen: ProfileAddEditStack,
            navigationOptions: (): NavigationScreenOptions => ({
                header: null,
                gesturesEnabled: false
            })
        }
    },
    {
        mode: 'modal',
        headerMode: 'screen'
    }
);

const AppContainer: NavigationContainer = createAppContainer(ModalStack);

export default AppContainer;