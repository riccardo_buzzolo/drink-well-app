import React, { Component } from 'react';
import { Text, StyleSheet, Dimensions, SafeAreaView, View } from 'react-native';
import { Color } from '../common/colors';
import { Ionicons } from '@expo/vector-icons';

class AppHeader extends Component {
    render() {
        return (
            <SafeAreaView style={{ backgroundColor: Color.PrimaryBlack }}>
                <View>
                    <View style={styles.container}>
                        <Ionicons style={styles.icon} name="ios-beer" size={32} color={Color.PrimaryWhite} />
                        <Text style={styles.title}>DrinkWell</Text>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: 60,
        width: Dimensions.get('screen').width,
        flexDirection: 'row',
        backgroundColor: Color.PrimaryBlack,
        alignItems: 'center'
    },
    icon: {
        marginLeft: 25,
        marginRight: 10
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        color: Color.PrimaryWhite
    }
});

export default AppHeader;