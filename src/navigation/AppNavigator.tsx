import React from 'react';
import { createMaterialTopTabNavigator, createAppContainer, NavigationContainer, NavigationScreenOptions, createStackNavigator} from 'react-navigation';
import { HomeScreen, ResultScreen, ProfileScreen } from '../components';
import { Ionicons } from '@expo/vector-icons';
import { Platform } from 'react-native';
import { Color } from '../common/colors';
import { TOP_TAB_WIDTH } from '../common/constants';
import ProfileAddEditScreen from '../components/ProfileAddEditScreen';

const AppNavigator: NavigationContainer = createMaterialTopTabNavigator(
    {
        Home: {
            screen: HomeScreen,
            navigationOptions: (): NavigationScreenOptions => ({
                headerTitle: 'Home',
                tabBarIcon: <Ionicons name={Platform.OS === 'ios' ? 'ios-home' : 'md-home'} size={22} color={Color.PrimaryWhite} />
            })
        },
        Profiles: {
            screen: ProfileScreen,
            navigationOptions: (): NavigationScreenOptions => ({
                headerTitle: 'Profiles',
                tabBarIcon: <Ionicons name={Platform.OS === 'ios' ? 'ios-people' : 'md-people'} size={22} color={Color.PrimaryWhite} />
            })
        },
        Result: {
            screen: ResultScreen,
            navigationOptions: (): NavigationScreenOptions => ({
                headerTitle: 'Result',
                tabBarIcon: <Ionicons name={Platform.OS === 'ios' ? 'ios-speedometer' : 'md-speedometer'} size={22} color={Color.PrimaryWhite} />
            })
        }
    },
    {
        initialLayout: {
            height: 40,
            width: TOP_TAB_WIDTH
        },
        tabBarOptions: {
            activeTintColor: Color.PrimaryWhite,
            inactiveTintColor: Color.PrimaryBlack,
            showIcon: true,
            showLabel: false,
            labelStyle: {
                fontSize: 14
            },
            tabStyle: {

            },
            indicatorStyle: {
                backgroundColor: Color.PrimaryOrange
            },
            style: {
                height: 40,
                width: TOP_TAB_WIDTH,
                backgroundColor: Color.PrimaryBlack,
                elevation: 4,
                shadowColor: '#000',
                shadowOpacity: 0.23,
                shadowRadius: 2.62,
                shadowOffset: {
                    width: 0,
                    height: 2
                }
            }
        }
    }
);

export default AppNavigator;